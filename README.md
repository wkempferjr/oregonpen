# README #



### What is this repository for? ###

* This is a demo repository. It contains the code for a child theme which, for now, houses a AngularJS-based user interface of a public notice application. This code is to eventually be moved to a WordPress plugin. 

* 0.0.1


### Who do I talk to? ###

* Wes Kempfer (wkempferjr@tnotw.com)