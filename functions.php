<?php
/**
 * User: weskempferjr
 * Date: 1/5/17
 * Time: 2:43 PM
 */

if (!defined('PAD_CHILD_THEME_TEXTDOMAIN'))
    define('PAD_CHILD_THEME_TEXTDOMAIN', 'pad-child');

require get_stylesheet_directory() . '/inc/class-pad-child-shortcodes.php';

require get_stylesheet_directory() . '/inc/class-pad-child-user-identifier.php';
require get_stylesheet_directory() . '/inc/class-pad-child-dashboard-configurator.php';
$dashboard_configurator = new PAD_Child_Dashboard_Configurator();
$dashboard_configurator->init_dashboard();

// If front end page and user is subsciber, disable toolbar
if ( PAD_Child_User_Identifier::is_notice_subscriber() && !is_admin() ) {
    add_filter('show_admin_bar', '__return_false');
}

function pad_child_enqueue_styles() {

    if ( ! has_pad_child_shortcodes() && ! has_awpcp_shortcodes()) {
        return;
    }


    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );

    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    wp_enqueue_script( 'pad-child-admin-tiny-mce',
        get_stylesheet_directory_uri() . '/js/tinymce/tinymce.min.js',
        array('jquery'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-custom-jsr',
        get_stylesheet_directory_uri() . '/js/custom.js',
        array('jquery'),
        wp_get_theme()->get('Version'),
        true
    );

    // Load angular resources
    wp_enqueue_script( 'pad-child-ng',
        get_stylesheet_directory_uri() . '/js/angular.js',
        array('jquery'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-ng-route',
        get_stylesheet_directory_uri() . '/js/angular-route.min.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-ng-animate',
        get_stylesheet_directory_uri() . '/js/angular-animate.min.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-ng-sanitize',
        get_stylesheet_directory_uri() . '/js/angular-sanitize.min.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-ng-resource',
        get_stylesheet_directory_uri() . '/js/angular-resource.min.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );
    
    wp_enqueue_script( 'pad-child-ng-ui-router',
        get_stylesheet_directory_uri() . '/js/angular-ui-router.min.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-ng-infinite-scroll',
        get_stylesheet_directory_uri() . '/js/ng-infinite-scroll.min.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );
    //add for spinner
    wp_enqueue_script( 'pad-child-ng-spinner',
        get_stylesheet_directory_uri() . '/js/angular-spinner.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-ng-ui-bootstrap',
        get_stylesheet_directory_uri() . '/js/ui-bootstrap.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-ng-ui-bootstrap-tpls',
        get_stylesheet_directory_uri() . '/js/ui-bootstrap-tpls.js',
        array('jquery', 'pad-child-ng'),
        wp_get_theme()->get('Version'),
        true
    );

    wp_enqueue_script( 'pad-child-ng-app',
        get_stylesheet_directory_uri() . '/js/public_notices.js',
        array('jquery','pad-child-ng'),
        null,
        true 
    );
    
    pad_child_ng_config();

    wp_localize_script( 'public-notice-config' , 'publicNoticesConfig', array(
        
        'configuration' => array(
            'partialsURL' => get_stylesheet_directory_uri() . '/partials',
            'searchURL' => '/search-notices/'
        ),

        'l10n' => array(

        )


    ));



}


add_action( 'wp_enqueue_scripts', 'pad_child_enqueue_styles' );




function pad_child_enqueue_admin_scripts () {


    wp_enqueue_script( 'pad-child-admin-tiny-mce',
        get_stylesheet_directory_uri() . '/js/tinymce/tinymce.min.js',
        array('jquery'),
        wp_get_theme()->get('Version'),
        true
    );



    wp_enqueue_script( 'pad-child-admin-custom-jsr',
        get_stylesheet_directory_uri() . '/js/custom.js',
        array('jquery'),
        wp_get_theme()->get('Version'),
        true
    );


}

// add_action( 'admin_enqueue_scripts', 'pad_child_enqueue_admin_scripts' );



if ( ! function_exists( 'pad_posted_on' ) ) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function pad_posted_on() {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf( $time_string,
            esc_attr( get_the_date( 'm/d/Y' ) ),
            esc_html( get_the_date('m/d/Y') ),
            esc_attr( get_the_modified_date( 'm/d/Y' ) ),
            esc_html( get_the_modified_date('m/d/Y') )
        );

        $posted_on =  $time_string ;


        echo '<span class="posted-on">' . $posted_on . '</span>';

    }
endif;


if ( ! function_exists( 'pad_entry_footer' ) ) :
    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function pad_entry_footer() {

        

        edit_post_link(
            sprintf(
            /* translators: %s: Name of current post */
                esc_html__( 'Edit %s', 'pad' ),
                the_title( '<span class="screen-reader-text">"', '"</span>', false )
            ),
            '<span class="edit-link">',
            '</span>'
        );
    }
endif;

// AWCWP configuration, modifications

function show_single_ad_filter( $content, $listing_id ) {

    $content = str_replace('Flag Ad', __('Flag Notice', PAD_CHILD_THEME_TEXTDOMAIN),  $content );
    $content = str_replace('This Ad has been viewed', __('This Notice has been viewed', PAD_CHILD_THEME_TEXTDOMAIN),  $content );

    // strip out br tags which are added by the awpcp plugin.
    $content = str_replace('<br />', '', $content );

    return $content;
}
add_filter( 'awpcp-show-ad', 'show_single_ad_filter', 10, 2 );

function change_ad_contact_name_label( $content ) {
    $content = str_replace("Name of Person to Contact",__("Publisher's Name", PAD_THEME_TEXTDOMAIN), $content);
    return $content;
}

function change_ad_contact_email_label( $content ) {
    $content = str_replace('Contact Person&#039;s Email',__("Publisher's Email", PAD_THEME_TEXTDOMAIN), $content);
    return $content;
}

function change_ad_contact_phone_label( $content ) {
    $content = str_replace('Contact Person&#039;s Phone Number',__("Publisher's Phone", PAD_THEME_TEXTDOMAIN), $content);
    return $content;
}

add_filter( 'awpcp-render-form-field-ad_contact_name', 'change_ad_contact_name_label' );
add_filter( 'awpcp-render-form-field-ad_contact_email', 'change_ad_contact_email_label' );
add_filter( 'awpcp-render-form-field-ad_contact_phone', 'change_ad_contact_phone_label' );





// Angularjs configuration
function pad_child_ng_config() {

    $_theme = wp_get_theme();
    $categories = pad_child_get_notice_categories();
    $regions = pad_child_get_regions() ;


    $config = array(
        'config' => array(
            'baseUrl'     => trailingslashit( get_home_url() ),
            'ajaxUrl'     => admin_url( 'admin-ajax.php' ),
            'local'       => get_locale(),
            'themeName'   => $_theme->get('Name'),
            'themeVersion'=> $_theme->get('Version'),
            'wpVersion'   => get_bloginfo('version'),
            'enableDebug' => (WP_DEBUG !== false) ? true : false,
            'env'         => defined('WP_ENV') ? WP_ENV : 'production',
            'modules'     => array(
                'publicNotices' => array(
                    'l10n' =>  array(
                        'searchTitle' => __( 'Search Notices', PAD_CHILD_THEME_TEXTDOMAIN ),
                    ),
                    'restNamespace' => 'v1/public-notice-route',   // Rest Api route
                    'partialUrl' => get_stylesheet_directory_uri() . '/partials/', //Template html url
                    'resourceRoot' => get_stylesheet_directory_uri() . '/',
                    'noticeCategories' => $categories,
                    'localizations' => pad_child_get_notice_localizations(),
                    'recordsPerRequest' => 5,
                    'showNoticeURL' => site_url() . '/another-wordpress-classifieds-plugin/show-notice/?id=',
                    'searchSlug' => 'search-public-notices',
                    'regions' => $regions
                )
            )
        )
    );


    $script = sprintf( 'window.wpNg = %s', json_encode( $config ) );
    wp_add_inline_script('pad-child-ng-app', $script, 'before');

}





function pad_child_get_notice_categories () {

    $category_array = array();
    if ( function_exists('awpcp_categories_collection') ) {
        $cat_collection = awpcp_categories_collection();
        $categories = $cat_collection->get_all();

    }

    foreach ( $categories as $category) {
        $category_array[] = $category->name ;
    }

    return $category_array ;
}


function pad_child_get_regions() {

    $top_level_region = 'Oregon';
    
    global $wpdb;


    $prepare_string = $wpdb->prepare("select region_id from wp_awpcp_regions where region_name = '%s'", $top_level_region );
    $top_level_region_res = $wpdb->get_row( $prepare_string );
    $top_level_region_id= $top_level_region_res->region_id ;



    $prepare_string = $wpdb->prepare("select region_id, region_name from wp_awpcp_regions where region_parent = %d ORDER BY region_name", $top_level_region_id );
    $counties = $wpdb->get_results( $prepare_string );



    $countiesArray = array();
    foreach ( $counties as $county )  {
        $county_name = $county->region_name;
        $co_region_id = $county->region_id;
        $prepare_string = $wpdb->prepare("select region_id, region_name from wp_awpcp_regions where region_parent = %d ORDER BY region_name", $co_region_id );
        $cities =  $wpdb->get_results( $prepare_string );


        $city_names = array();
        foreach ( $cities as $city ) {
            $city_names[] = $city->region_name ;

        }
        $county = array(
            'name' => $county_name,
            'cities' => $city_names
        );

        $countiesArray[] = $county ;
    }

    $regions = array(
        'counties' => $countiesArray
    );


    return $regions;


}


/* 
*TODO: more error checking in this function. If the purpose of the function_exists call is to do 
 * check if the awpcp plugin is installed, then should there be an error thwown if it isn't? 
*/
function pad_child_get_notice_category_objects () {

    $categories = array();
    if ( function_exists('awpcp_categories_collection') ) {
        $cat_collection = awpcp_categories_collection();
        $categories = $cat_collection->get_all();
    }

    return $categories;
}

function pad_child_get_notice_category_lookup () {

    $category_objects = pad_child_get_notice_category_objects();
    $category_id_lookup = array();
    foreach ( $category_objects as $cat_obj ) {
        $category_id_lookup[ $cat_obj->name ] = $cat_obj->id ;
    }

    return $category_id_lookup;
}

function pad_child_get_notice_localizations() {
    
    $pn_localizations = array(
        'categoryChecklistLabel' => __('Categories', PAD_CHILD_THEME_TEXTDOMAIN),
        'keywordsInputLabel' => __('Keywords', PAD_CHILD_THEME_TEXTDOMAIN),
        'dateRangeLabel' => __('Date Range', PAD_CHILD_THEME_TEXTDOMAIN),
        'dateFromLabel' => __('From date', PAD_CHILD_THEME_TEXTDOMAIN),
        'dateToLabel' => __('To date', PAD_CHILD_THEME_TEXTDOMAIN),
        'responsibleEntityLabel' => __('Responsible Entity', PAD_CHILD_THEME_TEXTDOMAIN),
        'dateRangeFormat' => __('MMMM dd, yyyy', PAD_CHILD_THEME_TEXTDOMAIN),
        'clickToEnlarge' => __('Click to enlarge image', PAD_CHILD_THEME_TEXTDOMAIN ),
        'closeLabel' => __('Close', PAD_CHILD_THEME_TEXTDOMAIN ),
        'categoryLabel' => __('Category', PAD_CHILD_THEME_TEXTDOMAIN ),
        'publishedDateLabel' => __('Published on:', PAD_CHILD_THEME_TEXTDOMAIN ),
        'readMoreLabel' => __('Expand', PAD_CHILD_THEME_TEXTDOMAIN ),
        'responsibleEntityContactInfoLabel' => __('Contact Information', PAD_CHILD_THEME_TEXTDOMAIN ),
        'responsibleEntityNameLabel' => __('Responsible Entity', PAD_CHILD_THEME_TEXTDOMAIN ),
        'representingAttorneyNameLabel' => __('Representing Attorney', PAD_CHILD_THEME_TEXTDOMAIN ),
        'filterByCategoryLabel' => __('Filter by Category', PAD_CHILD_THEME_TEXTDOMAIN ),
        'filterByKeywordsLabel' => __('Filter by Keywords', PAD_CHILD_THEME_TEXTDOMAIN ),
        'filterByDateRangeLabel' => __('Filter by Date Range', PAD_CHILD_THEME_TEXTDOMAIN ),
        'filterByRegionLabel' => __('Filter by County and City', PAD_CHILD_THEME_TEXTDOMAIN ),
        'filterFeedbackLabel' => __('Filter', PAD_CHILD_THEME_TEXTDOMAIN ),
        'filterFeedbackDefault' => __('None (Displaying all notices)', PAD_CHILD_THEME_TEXTDOMAIN ),
        'filterFeedbackCategoriesLabel' => __('By Category', PAD_CHILD_THEME_TEXTDOMAIN),
        'filterFeedbackKeywordsLabel' => __('Keywords', PAD_CHILD_THEME_TEXTDOMAIN),
        'filterFeedbackDateRangeLabel' => __('Date Range', PAD_CHILD_THEME_TEXTDOMAIN),
        'filterFeedbackCountiesLabel' => __('Counties', PAD_CHILD_THEME_TEXTDOMAIN),
        'filterFeedbackCitiesLabel' => __('Cities', PAD_CHILD_THEME_TEXTDOMAIN),
        'showArchivedLabel' => __('Show archived notices', PAD_CHILD_THEME_TEXTDOMAIN),
        'hideArchivedLabel' => __('Hide archived notices', PAD_CHILD_THEME_TEXTDOMAIN),
        'resetFilterLabel' => __('Reset Filter', PAD_CHILD_THEME_TEXTDOMAIN),








    );
    
    return $pn_localizations;
    
}

add_filter('wp_ng_publicNotices_config', 'config_ng_mod' );

function register_ng_modules ( $ng_modules ) {

    $ng_modules[] = 'publicNotices';

    return $ng_modules;
}
add_filter('wp_ng_register_ng_modules', 'register_ng_modules');


// AJAX
require get_stylesheet_directory() . '/inc/class-pad-child-search-handler.php';
require get_stylesheet_directory() . '/inc/class-pad-child-view-count-handler.php';
require get_stylesheet_directory() . '/inc/class-pad-child-ajax.php';

$ajax_controller = new Pad_Child_Ajax_Controller();
add_action('wp_ajax_nopriv_pad_child_ajax', array( $ajax_controller, 'pad_child_ajax'));
add_action('wp_ajax_pad_child_ajax', array( $ajax_controller, 'pad_child_ajax'));



// Short codes


function has_awpcp_shortcodes() {

    global $post;

    if ( $post == null ) return;

    $post_type = get_post_type();

    $has_shortcodes = false;

    if ( $post_type == "post"|| $post_type == "page" ) {

        foreach( array('AWPCPPLACEAD') as $shortcode ) {
            if ( has_shortcode( $post->post_content, $shortcode ) ) {
                $has_shortcodes = true;
                break;
            }
        }
    }
    return $has_shortcodes;
}

function has_pad_child_shortcodes() {

    global $post;

    if ( $post == null ) return;

    $post_type = get_post_type();

    $has_shortcodes = false;
    $pad_shortcodes = new PAD_Child_Shortcodes();
    
    if ( $post_type == "post"|| $post_type == "page" ) {

        foreach( $pad_shortcodes->get_shortcodes() as $shortcode ) {
            if ( has_shortcode( $post->post_content, $shortcode ) ) {
                $has_shortcodes = true;
                break;
            }
        }
    }
    return $has_shortcodes;
}




$pad_shortcodes = new PAD_Child_Shortcodes();
$pad_shortcodes->register();

function pad_child_account_menu_html()
{
    $output = '';
    if (is_user_logged_in()) {
        $title = __('Log out');
        $href = wp_logout_url( site_url() );
    } else {
        $title = __('Customer Login', PAD_THEME_TEXTDOMAIN);
        $href = wp_login_url( site_url( '/wp-admin/admin.php?page=awpcp-panel' ));
    }
    $output .= '<a title="' . $title . '" href="' . $href . '"><i class="fa fa-user"></i></a>';

    return $output;

}


/*
 * Cron jobs
 */

require get_stylesheet_directory() . '/inc/class-pad-child-notification-generator.php';

if (!wp_next_scheduled('check_for_expired_notices_hook')) {
    wp_schedule_event( time(), 'daily', 'check_for_expired_notices_hook' );
}
add_action ( 'check_for_expired_notices_hook', 'send_expired_notifications' );

function send_expired_notifications() {

    $notification_generator = new Pad_Child_Notification_Generator();
    try {
        $notification_generator->send_deactivation_notifications();
    }
    catch (Exception $e ) {
        error_log(__FILE__ . ',' . __LINE__ . ':' . $e->getMessage() );
    }
}



?>
