<?php

/**
 *  Ajax controller for child theme.
 */
class Pad_Child_Ajax_Controller
{

    public function execute_request() {
        
        try {
            switch($_REQUEST['fn']){
                case 'search_notices':
                    // If  not set, consider it an invalid request.
                    if ( !isset( $_REQUEST['searchCriteria'] ) ) {
                        throw new Exception(_e('Invalid search notices request.', PAD_CHILD_THEME_TEXTDOMAIN ) );
                    }
                    $searchCriteria = $this->sanatize_search_criteria( json_decode( stripslashes($_REQUEST['searchCriteria']), true, 10 ) );
                    $output = $this->search_notices( $searchCriteria );
                    if ( $output === false ) {
                        throw new Exception(__('Error searching for notices.', PAD_CHILD_THEME_TEXTDOMAIN ) );
                    }
                    break;

                case 'record_notice_views':
                    if ( !isset( $_REQUEST['noticesViewed'] ) ) {
                        throw new Exception(_e('Invalid record notice views request.', PAD_CHILD_THEME_TEXTDOMAIN ) );
                    }
                    $notices_viewed = $this->sanatize_notices_viewed( json_decode( stripslashes($_REQUEST['noticesViewed']), true, 10 ) );
                    $output = $this->record_notice_views( $notices_viewed );
                    if ( $output === false ) {
                        throw new Exception(__('Error recording views.', PAD_CHILD_THEME_TEXTDOMAIN ) );
                    }
                    break;
                default:
                    $output = __('Unknown ajax request sent from client.', PAD_CHILD_THEME_TEXTDOMAIN );
                    break;
            }
            
        }
        catch ( Exception $e ) {
            $errorData = array(
                'errorData' => 'true',
                'errorMessage' => $e->getMessage(),
                'errorTrace' => $e->getTraceAsString()
            );
            $output = $errorData;
        }
        
        $output=json_encode($output);
        if(is_array($output)){
            print_r($output);
        }
        else {
            echo  $output ;
        }
        die;
    }

    public function pad_child_ajax() {
       $this->execute_request();
    }
    
    public function search_notices( $searchCriteria ) {
        
        $search_handlder = new Pad_Child_Search_Handler();
        return $search_handlder->search_notices( $searchCriteria );
    }

    public function record_notice_views( $notices_viewed ) {
        
        $view_count_handler = new Pad_Child_View_Count_Handler();
        return $view_count_handler->record_notice_views( $notices_viewed) ;
    }
    
    private function sanatize_search_criteria ( $searchCriteria) {

        // TODO: need to traverse selectedNoticeCatories to validate.

        // TODO: this function is where country and state are hardcoded. Change to roll out to other states or countries.
        
        $scrubbedSearchCriteria = array(
            'fromDate' => sanitize_text_field( $searchCriteria['fromDate']),
            'toDate' => sanitize_text_field( $searchCriteria['toDate']),
            'keywords' => sanitize_text_field( $searchCriteria['keywords']),
            'recordsPerRequest' => intval( $searchCriteria['recordsPerRequest']),
            'recordOffset' => intval( $searchCriteria['recordOffset']),
            'selectedNoticeCategories' => $searchCriteria[ 'selectedNoticeCategories'],
            'city' => sanitize_text_field( $searchCriteria['city']),
            'county' => sanitize_text_field( $searchCriteria['county']),
            'state' => 'Oregon',
            'country' => 'USA',
            'includeArchived' => boolval( $searchCriteria['includeArchived'] )

        );

        return $scrubbedSearchCriteria ;
    }

    private function sanatize_notices_viewed ( $noticesViewed ) {

        $scrubbed_notices_viewed = array();
        foreach ( $noticesViewed as $noticeViewed ) {
            $scrubbed_notices_viewed[] = intval( $noticeViewed );
        }

        return $scrubbed_notices_viewed;

    }

}