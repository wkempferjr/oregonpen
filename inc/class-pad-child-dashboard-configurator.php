<?php

/**
 * Created by PhpStorm.
 * User: weskempferjr
 * Date: 6/5/17
 * Time: 4:33 PM
 */
class PAD_Child_Dashboard_Configurator
{

    public function init_dashboard() {

        if ( PAD_Child_User_Identifier::is_notice_subscriber() ) {
            add_action('admin_menu', array($this, 'configure_menu_pages'), 999);
            add_action('admin_init', array($this, 'remove_default_widgets'));
            add_action( 'wp_dashboard_setup', array($this, 'add_subscriber_widgets') );
            add_action( 'wp_before_admin_bar_render', array($this, 'configure_subscriber_toolbar') );
        }
    }

    public function configure_menu_pages() {
        remove_menu_page('jetpack');
    }

    public function remove_default_widgets() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
        remove_meta_box( 'jetpack_summary_widget', 'dashboard', 'normal');
        remove_meta_box( 'wpe_dify_news_feed', 'dashboard', 'normal');

    }

    public function add_subscriber_widgets(){

        wp_add_dashboard_widget(
            'subscriber_overview_widget',         // Widget slug.
            'Subcriber Dashboard',         // Title.
            array( $this, 'subscriber_overview_widget') // Display function.
        );
    }


    public function subscriber_overview_widget() {
        $output = '<h1>' . __('Subcriber Overview', PAD_CHILD_THEME_TEXTDOMAIN) . '</h1>';
        $output .= '<p>' . __('Click on the Ad Management link to add or edit notices.', PAD_CHILD_THEME_TEXTDOMAIN) . '</p>';
        $output .= '<p>' . __('Click on the Profile link to edit your profile.', PAD_CHILD_THEME_TEXTDOMAIN) . '</p>';
        echo $output ;
    }

    public function configure_subscriber_toolbar() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('wp-logo');

        $wp_admin_bar->add_menu( array(
                'id' => 'search-public-notices',
                'title' => __('Search Public Notices', PAD_CHILD_THEME_TEXTDOMAIN),
                'href' => '/search-public-notices',
                'meta' => false,
                'parent' => false
            )
        );

    }




    
}