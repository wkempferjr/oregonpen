<?php

/**
 * Created by PhpStorm.
 * User: weskempferjr
 * Date: 5/19/17
 * Time: 11:37 AM
 */
class Pad_Child_Notification_Generator
{


    /**
     * Pad_Child_Notification_Generator constructor.
     */
    public function __construct()
    {
    }

    public function send_deactivation_notifications() {
        
        global $wpdb ;
        $wpdb->hide_errors();

        // Get a list of the notices that have expired
        $prepare_string = $wpdb->prepare('select a.ad_id, c.category_name, a.ad_title, a.ad_details, a.ad_startdate, a.ad_enddate, a.ad_contact_email, a.user_id, u.user_login, u.user_nicename, u.user_email from wp_awpcp_ads a, wp_users u, wp_awpcp_categories c where a.user_id = u.ID AND a.ad_category_id = c.category_id AND date( ad_enddate ) = CURDATE() AND a.ad_id > %d',0);
        $raw_notices  = $wpdb->get_results( $prepare_string );

        if ( $wpdb->last_error ) {
            throw new Exception(__("Query for expired notices failed", PAD_CHILD_THEME_TEXTDOMAIN));
        }

        // TODO: create theme setting for admin email addresses)
        $recipients = 'wkempferjr@gmail.com';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        foreach ( $raw_notices as $raw_notice) {

            $message_contents = '<html><body>';
            //   Get user name, user email, start date, end date (should be current date), ad title, ad contents
            //   Ad the above to mail message body
            $message_contents .= '<h1>'. __('Proof of Notification', PAD_CHILD_THEME_TEXTDOMAIN) .'</h1>';
            $message_contents .= '<p>' . __('Published by ', PAD_CHILD_THEME_TEXTDOMAIN) . ': ' . $raw_notice->user_nicename . '</p>';
            $message_contents .= '<p>' . __('Publisher email ', PAD_CHILD_THEME_TEXTDOMAIN) . ': ' .  $raw_notice->user_email . '</p>';
            $message_contents .= '<p>' . __('Notice category ', PAD_CHILD_THEME_TEXTDOMAIN) . ': ' . $raw_notice->category_name . '</p>';
            $message_contents .= '<p>' . __('Start Date ', PAD_CHILD_THEME_TEXTDOMAIN) . ': ' . $raw_notice->ad_startdate . '</p>';
            $message_contents .= '<p>' . __('End Date ', PAD_CHILD_THEME_TEXTDOMAIN) . ': ' . $raw_notice->ad_enddate . '</p>';
            $message_contents .= '<p>' . __('Notice content is as follows:', PAD_CHILD_THEME_TEXTDOMAIN) . '</p>';
            $message_contents .= '<h2>'. $raw_notice->ad_title . '</h2>';
            $message_contents .= $raw_notice->ad_details ;
            $message_contents .= '</body></html>';

            $subject = __('Proof of notification, notice ', PAD_CHILD_THEME_TEXTDOMAIN) . $raw_notice->ad_id ;

            wp_mail( $recipients, $subject, $message_contents, $headers );

        }

    }
}