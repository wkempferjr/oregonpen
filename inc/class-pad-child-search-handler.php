<?php

/**
 * 
 */
class Pad_Child_Search_Handler
{

    private $ignore_category_filter = true;
    // TODO: sanity check searchCriteria


    public function search_notices( $searchCriteria ) {

        // Get the date range.
        $from_date = $searchCriteria['fromDate'];
        if ( empty( $from_date ) ) {
            $from_date_string = '1970-1-1 00:00:00';
        }
        else {
            $from_date_time = strtotime($from_date);
            $from_date_string = date('Y-m-d 00:00:00', $from_date_time);
        }

        $to_date = $searchCriteria['toDate'];
        if ( empty( $to_date )) {
            $to_date_string = '2070-1-1 00:00:00';
        }
        else {
            $to_date_time = strtotime( $to_date );
            $to_date_string = date( 'Y-m-d 00:00:00',$to_date_time);
        }

        // Get the keyword arguments
        $keywords = $searchCriteria['keywords'];
        
        $records_per_request = $searchCriteria['recordsPerRequest'];
        $record_offset = $searchCriteria['recordOffset'];

        // Get the categories
        $search_categories = $searchCriteria['selectedNoticeCategories'];



        $category_query = '';
        if ( count( $search_categories ) > 0 && ! $this->ignore_category_filter ) {
            $categories = pad_child_get_notice_category_lookup();

            $category_query = ' AND ad_category_id in (';
            foreach ($search_categories as $search_category) {
                $category_query .= $categories[$search_category] . ', ';
            }
            $category_query = rtrim($category_query, ', ');
            $category_query .= ') ';
        }

        // Get region info
        // Currently country and state are pegged to USA and Oregon, respectively, in Pad_Child_Ajax_Handler->sanitize_search_criteria
        $country = $searchCriteria['country'];
        $state = $searchCriteria['state'];
        $state_query = ' AND r.state = "' . $state  . '"';
        $country_query = ' AND r.country = "' . $country . '"' ;

        $county_query = '';
        $search_counties = $searchCriteria['selectedCounties'];
        if ( count( $search_counties ) > 0 ) {

            $county_query = ' AND r.county in (';
            foreach ($search_counties as $search_county) {
                $county_query .= $search_county . ', ';
            }
            $county_query = rtrim($county_query, ', ');
            $county_query .= ') ';
        }


        $city_query = '';
        $search_cities = $searchCriteria['selectedCities'];
        if ( count( $search_cities ) > 0 ) {

            $city_query = ' AND r.county in (';
            foreach ($search_cities as $search_city ) {
                $city_query .= $search_city . ', ';
            }
            $city_query = rtrim($city_query, ', ');
            $city_query .= ') ';
        }

        /*
         * End Date query deterimines if archived notices are included.
         * The default is to exclude inactive (expired) checking
         * the ad end date with the current date.
        */
        $end_date_query = ' AND ( a.ad_enddate >= CURDATE() ) ';
        if ( $searchCriteria['includeArchived'] ) {
            $end_date_query = '';
        }

        global $wpdb ;
        $wpdb->hide_errors();


        $prepare_string = $wpdb->prepare(
            "select a.*, r.country, r.state, r.county, r.city from wp_awpcp_ads a, wp_awpcp_ad_regions r where 
                ( a.ad_startdate >= '%s' and a.ad_startdate < '%s' ) AND
                 ( a.ad_details like '%%%s%%' OR a.ad_title like '%%%s%%' OR a.responsible_entity_companyorganization like '%%%s%%'   ) AND a.ad_id = r.ad_id 
              " . $category_query . $country_query . $state_query . $county_query . $city_query . $end_date_query .' ORDER BY ad_startdate DESC LIMIT ' . $records_per_request . ' OFFSET ' . $record_offset ,
            $from_date_string,
            $to_date_string,
            $keywords,
            $keywords,
            $keywords
        );


        $raw_notices  = $wpdb->get_results( $prepare_string );
        if ( $wpdb->last_error ) {
            throw new Exception(__("Query for notices failed", PAD_CHILD_THEME_TEXTDOMAIN));
        }

        /*
         * The query above returns mulitple records for those notices that have mulitple images.
         * This code segement creates a single object for each on of them, preserving the mulitple image
         * paths as an array beloning to the notice object.
         */

        $notices = array();
        foreach ( $raw_notices as $raw_notice) {


            // TODO: $res_path for images should be a configurable or at least a global constant
            $res_path = '/wp-content/uploads/awpcp/';

            // Makes sure start date and end date conform to ISO 8601.
            $raw_notice->ad_startdate = str_replace(' ', 'T', $raw_notice->ad_startdate );
            $raw_notice->ad_enddate = str_replace(' ', 'T', $raw_notice->ad_enddate );


            $notices[ $raw_notice->ad_id ] = array(
                'notice' => $raw_notice,
                'media' => array(),
                'mediaLarge' => array(),
                'excerpt' => substr( strip_tags ( $raw_notice->ad_details ), 0, 150 ),
                'thumb' => '/wp-content/plugins/another-wordpress-classifieds-plugin/resources/images/adhasnoimage.png',
                'thumbLarge' => '/wp-content/plugins/another-wordpress-classifieds-plugin/resources/images/adhasnoimage.png',
                'thumbs' => array(),
                'thumbsLarge' => array()

            );

            // Get media paths for ad
            $media_prepare = $wpdb->prepare("select path from wp_awpcp_media where ad_id = %d", $raw_notice->ad_id );
            $media_results = $wpdb->get_results( $media_prepare );
            // $notices[ $raw_notice->ad_id ]['media'][] = $media_paths;

            foreach ( $media_results as $media_result ) {
                $notices[ $raw_notice->ad_id ]['media'][] =  $res_path . $media_result->path;
                $notices[ $raw_notice->ad_id ]['mediaLarge'][] = $res_path . preg_replace('/\.(jpg|png|gif|jpeg)$/',  '-large${0}', $media_result->path);

            }

            $image_count = count( $media_results );
            for ( $i = 0 ; $i < $image_count ; $i++ ){
                $notices[ $raw_notice->ad_id ]['thumbs'][] = preg_replace('/\/images\//', '/thumbs/', $notices[ $raw_notice->ad_id ]['media'][$i])  ;
                $notices[ $raw_notice->ad_id ]['thumbsLarge'][] =  $notices[ $raw_notice->ad_id ]['mediaLarge'][$i] ;

                if ( $i === 0 ) {
                    $notices[ $raw_notice->ad_id ]['thumb'] = preg_replace('/\/images\//', '/thumbs/', $notices[ $raw_notice->ad_id ]['media'][0])  ;
                    $notices[ $raw_notice->ad_id ]['thumbLarge'] =  $notices[ $raw_notice->ad_id ]['mediaLarge'][0] ;

                }
            }


            // Ad category name
            $cat_prepare = $wpdb->prepare("select category_name from wp_awpcp_categories where category_id = %d", $raw_notice->ad_category_id );
            $cat_name = $wpdb->get_row( $cat_prepare );
            $notices[ $raw_notice->ad_id ]['category'] = $cat_name->category_name ;

            // Stripped ad details for keyword filtering on the client side.
            $notices[ $raw_notice->ad_id ]['strippedDetails'] = strip_tags( $raw_notice->ad_details ) ;

         }

        return $notices;
    }


    public function set_ignore_category_filter( $ignore ) {
        $this->ignore_category_filter = $ignore;
    }
}