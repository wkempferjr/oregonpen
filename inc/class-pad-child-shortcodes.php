<?php

/**
 * PAD shortcode class
 */
class PAD_Child_Shortcodes
{

    private static $one_third_first = true;

    /**
     * PAD_Shortcodes constructor.
     */
    public function __construct()
    {
        
    }
    
    
    public function register() {
        add_shortcode('search_notices', array( $this, 'search_notices'));
    }


    public function search_notices( $atts, $content  ) {

        /** @var  $id */
        /** @var  $template */

        $atts_actual = shortcode_atts(
            array(
                'id'                => '',
                'template'    => ''
            ),
            $atts );


        extract( $atts_actual );

        $output = file_get_contents(  plugin_dir_path( __FILE__ ) . '../partials/public_notices.html' );
        

        return $output ;

    }


    public function get_shortcodes() {
        return array('search_notices');
    }
    
    


}