<?php

/**
 * Created by PhpStorm.
 * User: weskempferjr
 * Date: 6/6/17
 * Time: 12:48 PM
 */
class PAD_Child_User_Identifier
{
    public static function is_notice_subscriber() {
        
        $current_user = wp_get_current_user();
        $user_roles = $current_user->roles;
        if ( count( $user_roles) && in_array('subscriber', $user_roles) ) {
            return true;
        }
        
        return false;
    }
}