<?php

/**
 * User: weskempferjr
 * Date: 5/22/17
 * Time: 5:01 PM
 */
class Pad_Child_View_Count_Handler
{
    
    public function record_notice_views( $notice_id_list ) {

        global $wpdb ;
        $wpdb->hide_errors();

        $views_recorded = array();

        foreach ( $notice_id_list as $notice_id ) {
            $prepare_string = $wpdb->prepare('select ad_id, ad_views from wp_awpcp_ads where ad_id = %d', $notice_id);
            $row = $wpdb->get_row( $prepare_string );
            if ( $row != null ) {

                $n_views = $row->ad_views + 1;
                $ret_val = $wpdb->update(
                    'wp_awpcp_ads',
                    array(
                        'ad_views' => $n_views
                    ),
                    array(
                        'ad_id' => $notice_id
                    )
                );

                if ( $ret_val === false ) {
                    error_log(__FILE__ . ',' . __LINE__ . ':' . __('Could not update view count', PAD_CHILD_THEME_TEXTDOMAIN));
                    return false ;
                }
                else {
                    $views_recorded[] = $notice_id;
                }
            }
        }

        return $views_recorded;
        
    }

}