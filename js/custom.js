/**
 * Created by weskempferjr on 1/25/17.
 */
jQuery(document).ready(function() {
   jQuery('.awpcp-extra-field-title').before('<div class="notice-header"><h3>Contact Information for Responsible Entity</h3><h4>The Responsible Entity is the person or organization responsible for the content of this notice.</h4></div>');
   jQuery('.awpcp-extra-field-pusblisher_is_same_as_responsible_entity').before('<div class="notice-header"><h3>Contact Information for Publisher of Notice</h3><h4>The Publisher is the person or organization posting and paying for this notice.</h4></div>');
   jQuery('.awpcp-extra-field-responsible_entity_lawyer_status').before('<div class="notice-header"><h3>Information for Representing Attorney</h3><h4>The Representing Attorney is the person representing the party responsible for this notice.</h4></div>');
   // phone format in display view
   jQuery(".phone").each(function () { 
        var phone = jQuery(this).text();
        console.log(phone);
        var replacePhone = phone.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
        console.log(replacePhone);
        jQuery(this).text(replacePhone);
    });
});
(function($) {
    "use strict"; // Start of use strict

    tinymce.init({
        selector: '#ad-details',
        theme: 'modern',
        plugins: 'lists',
        forced_root_block : 'p',
        remove_trailing_brs: true,
        remove_linebreaks: true,
        valid_elements : ""
        +"a[href|target],"
        +"b,"
        +"br,"
        +"font[color|face|size],"
        +"img[src|id|width|height|align|hspace|vspace],"
        +"i,"
        +"ol,"
        +"ul,"
        +"li,"
        +"p[align|class],"
        +"h1,"
        +"h2,"
        +"h3,"
        +"h4,"
        +"h5,"
        +"h6,"
        +"span[class],"
        +"textformat[blockindent|indent|leading|leftmargin|rightmargin|tabstops],"
        +"u"
    });

    // format into phone number layout
    $('#awpcp-responsible_entity_primary_phone_number').keyup(function () {
        this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
    //alert ("OK");
    });
   $('#awpcp-responsible_entity_alternative_phone_number').keyup(function () {
        this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
    //alert ("OK");
    });
    // add parameters to image instructions
    $('<p>Image must be a minimum width of 640 pixels and min-height of 480px.</p>').appendTo('.awpcp-media-uploader-restrictions');
    //var contactHomePhone = $('#ad-contact-phone').val();
    //var contactMobilePhone = $('#ad-contact-phone').val();
    //var contactCompanyOrg = $('').val();

    //boxes are set by default to unchecked.
     $('.awpcp-extra-field-responsible_entity_is_same_as_contact_info input[type="checkbox"]').prop('checked', false);
     $('.awpcp-extra-field-responsible_entity_lawyer_status input[type="checkbox"]').prop('checked', false);
 
     $('.awpcp-extra-field-pusblisher_is_same_as_responsible_entity input[type="checkbox"]').change(function(e){
        e.preventDefault();

        // If responsible entity contact info is same as publisher hide contact 
        // fill in fields to match
        var REName  = $('#awpcp-responsible_entity_name').val();
        var REEmail = $('#awpcp-responsible_entity_email_address').val();
        var REStreetAddress1 = $('#awpcp-responsible_entity_street_address_1').val();
        var REStreetAddress2 = $('#awpcp-responsible_entity_street_address_2').val();
        var RECity = $("select.multiple-region[id^='city']").val();
        var REState = $("select.multiple-region[id^='state']").val();
        
        var REZipCode = $('#awpcp-responsible_entity_your_zipcode').val();
        var REPrimaryPhone = $('#awpcp-responsible_entity_primary_phone_number').val();
        var REAlternativePhone = $('#awpcp-responsible_entity_alternative_phone_number').val();
        console.log(REName);
        console.log(REEmail);
        console.log(REPrimaryPhone);
 
        var contactZipCode = $('#awpcp-contact_zip_code').val();
        
        if ( $(this).is(":checked") ){
        
            $('#ad-contact-name').val(REName);
            $('#ad-contact-email').val(REEmail);
            $('#awpcp-publisher_street_address1').val(REStreetAddress1);
            $('#awpcp-publisher_street_address2').val(REStreetAddress2);
            $('#awpcp-publisher_city').val(RECity);
            $('#awpcp-publisher_state').val(REState);
            $('#awpcp-publisher_zip_code').val(REZipCode);
            $('#ad-contact-phone').val(REPrimaryPhone);
        }
    }); 
    $('.awpcp-extra-field-responsible_entity_lawyer_status input[type="checkbox"]').change(function(e){
            e.preventDefault();
            if ( $(this).is(":checked") ){
                $('#awpcp-representing_attorney_full_name').val('N/A');
                $('#awpcp-representing_attorney_bar_number').val('N/A');
                // $('#awpcp-representing_attorney_email').val('N/A');
                // $('#awpcp-representing_attorney_address_1').val('N/A');
                // $('#awpcp-representing_attorney_address_2').val('N/A');
                // $('#awpcp-representing_attorney_city').val('N/A');
                // $('#awpcp-representing_attorney_state').val('N/A');
                // $('#awpcp-representing_attorney_zip_code').val('N/A');

            }
              //Change input required so it can be submitted with NA   
                $('form#adpostform.awpcp-details-form').submit(function (e) {
                    e.preventDefault();
                    $("#awpcp-representing_attorney_email").attr("data-val","false");
                    $("#awpcp-representing_attorney_email").attr("aria-required","false");
                    $('#awpcp-representing_attorney_email').removeAttr('data-val');
                    $('#awpcp-representing_attorney_email').removeAttr('aria-required');
                 }); 
    }); 
$('<strong>  Read more...</strong>').appendTo('.excerpt');
$("#search-notices #pn-category-checkbox label").on('change', function() {
         // $(this).find("input").prop('checked',
         //    function(i, oldVal) { return !oldVal; });
         $(this).toggleClass('highlighted');
         
     });
})(jQuery); // End of use strict

    
 
