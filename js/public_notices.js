/**
 * Created by weskempferjr on 2/8/17.
 */


( function(angular, wpNg) {
    'use strict';

    //var app = angular.module(wpNg.appName);
   var app = angular.module('publicNoticesApp', ["ngRoute","ngSanitize","ngAnimate","ngResource","ui.bootstrap","ui.router","infinite-scroll", "angularSpinner"]);


    app.config(function ($routeProvider, $locationProvider) {

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false,
                rewriteLinks: false
            });


            $routeProvider
                .when('/' + wpNg.config.modules.publicNotices.searchSlug + '/', {
                    templateUrl: wpNg.config.modules.publicNotices.partialUrl + 'search_notices.html',
                    controller: 'publicNoticeSearchController',
                    controllerAs: 'publicNoticeSearch'
                });



        })
             
               
        .controller('publicNoticeSearchController',

            ['$scope', '$route', '$routeParams', '$location', '$uibModal', 'searchService','$sce', 'usSpinnerService', '$interval', 'viewCountService',
                function ($scope, $route, $routeParams, $location, $uibModal, searchService, $sce, usSpinnerService, $interval, viewCountService ) {

                    // Start initilization
                    $scope.publicNoticeSearch = this;
                    $scope.publicNoticeSearch.request = {};


                    $scope.publicNoticeSearch.searchCriteria = '';
                    $scope.publicNoticeSearch.showNoticeURL = wpNg.config.modules.publicNotices.showNoticeURL;
                    $scope.publicNoticeSearch.noticeCategories = wpNg.config.modules.publicNotices.noticeCategories;
                    $scope.publicNoticeSearch.regions = wpNg.config.modules.publicNotices.regions;

                    // Flags used by uib-collapse in counties collapsible panels
                    $scope.publicNoticeSearch.countyCollapses = {};

                    var counties = $scope.publicNoticeSearch.regions.counties;
                    for ( var i = 0 ; i < counties.length ; i++ ) {
                        $scope.publicNoticeSearch.countyCollapses[ counties[i].name ] = true;
                    }

                    $scope.publicNoticeSearch.localizations = wpNg.config.modules.publicNotices.localizations;
                    $scope.publicNoticeSearch.request.recordsPerRequest = wpNg.config.modules.publicNotices.recordsPerRequest;
                    $scope.publicNoticeSearch.request.recordOffset = 0;



                    // Data picker config
                    var now = new Date();
                    $scope.publicNoticeSearch.today = now;
                    $scope.publicNoticeSearch.maxDate = new Date(2050, 12, 31);
                    $scope.publicNoticeSearch.minDate = new Date(1970, 1, 1);
                    $scope.publicNoticeSearch.request.fromDate = "";
                    $scope.publicNoticeSearch.request.toDate = "";
                    $scope.publicNoticeSearch.request.keywords = "";


                    $scope.publicNoticeSearch.dateRangeFormat = $scope.publicNoticeSearch.localizations.dateRangeFormat;
                    $scope.publicNoticeSearch.request.selectedNoticeCategories = [];
                    $scope.publicNoticeSearch.request.selectedCounties = [];
                    $scope.publicNoticeSearch.request.selectedCities = [];
                    
                    // Include archived notices in search
                    $scope.publicNoticeSearch.request.includeArchived = false;
                    

                    //populated by searchNotices()
                    $scope.publicNoticeSearch.notices = [];
                    $scope.publicNoticeSearch.loadInProgress = false;

                    angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 5000);

                    // Accordian open flags
                    $scope.publicNoticeSearch.categoryStatus = {
                        open: false
                    };
                    $scope.publicNoticeSearch.keywordsStatus = {
                        open: false
                    };
                    $scope.publicNoticeSearch.dataRangeStatus = {
                        open: false
                    };
                    $scope.publicNoticeSearch.regionStatus = {
                        open: false
                    };
                    
                    $scope.publicNoticeSearch.filterFeedback = {
                        categories : '',
                        counties : '',
                        cities : '',
                        dateRange: '',
                        keywords: '',
                        catFilterOn : false, 
                        cityFilterOn : false,
                        countyFilterOn : false,
                        dateRangeFilterOn: false,
                        keywordsFilterOn: false,
                        defaultOn : true,

                        reset : function() {
                            this.categories = '';
                            this.cities = '';
                            this.counties = '';
                            this.dateRange = '';
                            this.keywords = '';
                            this.catFilterOn = this.cityFilterOn = this.countyFilterOn = this.dateRangeFilterOn = this.keywordsFilterOn = false;
                            this.defaultOn = true;
                        }
                    }



                    // End initilization


                    // Start controller methods
                    $scope.today = function () {
                        $scope.dt = new Date();
                    };
                    $scope.today();

                    $scope.clearDate = function () {
                        $scope.dt = null;
                    };


                    $scope.datePickerInlineOptions = {
                        customClass: getDayClass,
                        minDate: new Date(),
                        showWeeks: true
                    };

                    $scope.fromDatePickerOptions = {
                        formatYear: 'yy',
                        maxDate: $scope.publicNoticeSearch.maxDate,
                        minDate: $scope.publicNoticeSearch.minDate,
                        startingDay: 1
                    };

                    $scope.toDatePickerOptions = {
                        formatYear: 'yy',
                        maxDate: $scope.publicNoticeSearch.maxDate,
                        minDate: $scope.publicNoticeSearch.minDate,
                        startingDay: 1
                    };

                    $scope.dateFromOpen = function () {
                        $scope.fromDatePicker.opened = 1;
                    }

                    $scope.dateToOpen = function () {
                        $scope.toDatePicker.opened = 1;
                    }

                    $scope.fromDatePicker = {
                        opened: false
                    };

                    $scope.toDatePicker = {
                        opened: false
                    };

                    function getDayClass(data) {
                        var date = data.date,
                            mode = data.mode;
                        if (mode === 'day') {
                            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                            for (var i = 0; i < $scope.events.length; i++) {
                                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                                if (dayToCheck === currentDay) {
                                    return $scope.events[i].status;
                                }
                            }
                        }

                        return '';
                    }

                    $scope.publicNoticeSearch.toggleShowArchived = function() {

                        // When switching between show/hide archived, start over but keep filter.
                        $scope.publicNoticeSearch.request.recordOffset = 0;
                        $scope.publicNoticeSearch.noMoreRecords = 0;

                        $scope.publicNoticeSearch.notices = [];
                        $scope.publicNoticeSearch.searchNotices();
                    }

                    $scope.publicNoticeSearch.resetFilter = function() {

                        $scope.publicNoticeSearch.request.fromDate = "";
                        $scope.publicNoticeSearch.request.toDate = "";
                        $scope.publicNoticeSearch.request.keywords = "";

                        $scope.publicNoticeSearch.request.selectedNoticeCategories = [];
                        $scope.publicNoticeSearch.request.selectedCities = [] ;
                        $scope.publicNoticeSearch.request.selectedCounties = [] ;

                        $scope.publicNoticeSearch.filterNotices();
                    }



                    // Search notices function
                    $scope.publicNoticeSearch.searchNotices = function () {

                        if (($scope.publicNoticeSearch.loadInProgress === true) || ($scope.publicNoticeSearch.noMoreRecords === true)) {
                            return;
                        }
                     

                        $scope.publicNoticeSearch.loadInProgress = true;
                        $scope.publicNoticeSearch.noMoreRecords = false;

                        searchService.searchNotices($scope)
                            .then(function (data) {


                                if (data.data.errorData != null && data.data.errorData == "true") {
                                    $scope.publicNoticeSearch.errorMessage = data.errorMessage;
                                    $scope.publicNoticeSearch.errorContainerClass = "displayed-section";
                                    // Set loadInProgress to true in order to prevent the infinite scroll module from thrashing.
                                    //$scope.publicNoticeSearch.loadInProgress = true;
                                    $scope.publicNoticeSearch.noMoreRecords = true;
                                    console.log("Request to server for public notices failed.");
                                }
                                else {
                                    // If the server has no more records, it won't return any. Disable infinite scroll in that case.
                                    // Notice Factory
                                    var dataArray = [];
                                    angular.forEach(data.data, function (notice) {
                                        notice.startDate = new Date( notice.notice.ad_startdate ) ;
                                        notice.endDate = new Date( notice.notice.ad_enddate ) ;
                                        notice.listAsCollapsed = true;
                                        notice.showListPanel = true;
                                        notice.viewCount = 0;
                                        notice.viewsReported = false;
                                        dataArray.splice(0, 0, notice);
                                    });

                                    if (dataArray.length === 0) {
                                        $scope.publicNoticeSearch.loadInProgress = false;
                                        $scope.publicNoticeSearch.noMoreRecords = true;
                                        return;
                                    }

                                    // Default sort order is ad_startdate.
                                    if (dataArray.length > 1) {
                                        dataArray.sort(function (a, b) {
                                            var aTime = new Date(a.notice.ad_startdate).getTime();
                                            var bTime = new Date(b.notice.ad_startdate).getTime();
                                            return bTime - aTime;
                                        })
                                    }

                                    angular.forEach(dataArray, function (notice) {
                                        $scope.publicNoticeSearch.notices.splice(0, 0, notice);
                                       
                                    });
                 
                                    $scope.publicNoticeSearch.filterNotices();

                                    var newArray = dataArray;
                                    $scope.newArray = newArray;
                                    for(var i=0; i < $scope.newArray.length; i++){        
                                        $scope.listing = ($scope.newArray[i].notice);
                                        // console.log($scope.listing);
                                        $scope.listing_details = ($scope.newArray[i].notice.ad_details);
                                        $scope.listingHTML = $scope.listing_details;
                                    };

                                    $scope.publicNoticeSearch.request.recordOffset = parseInt($scope.publicNoticeSearch.request.recordOffset) + parseInt($scope.publicNoticeSearch.request.recordsPerRequest);

                                    if ($scope.publicNoticeSearch.notices.length > 0) {
                                        $scope.publicNoticeSearch.listOpenButtonClass = "displayed-section";
                                        $scope.publicNoticeSearch.noticeListClass = "displayed-section";
                                        $scope.publicNoticeSearch.errorContainerClass = "hidden-section";

                                    }
                                    else {
                                        // $scope.publicNoticeSearch.errorMessage = $scope.publicNoticeSearch.noNotciesFoundMessage ;
                                        $scope.publicNoticeSearch.errorContainerClass = "displayed-section";
                                    }

                                    $scope.publicNoticeSearch.loadInProgress = false;


                                }

                            }, function (data) {
                                console.log('Error on notice search');
                                $scope.publicNoticeSearch.loadInProgress = false;
                            });
                    }

                    $scope.publicNoticeSearch.recordNoticeViews = function() {

                        // For each notice, get view count and send update to server.
                        $scope.publicNoticeSearch.noticesViewed = [] ;
                        for ( var i = 0 ; i < $scope.publicNoticeSearch.notices.length ; i++ ) {
                            var notice = $scope.publicNoticeSearch.notices[i]
                            if ( notice.viewCount > 0 && notice.viewsReported === false ) {
                                $scope.publicNoticeSearch.noticesViewed.splice(0,0, notice.notice.ad_id );
                            }

                        }

                        // If nothing view, just return.
                        if ($scope.publicNoticeSearch.noticesViewed.length === 0  ) return;

                        viewCountService.recordNoticeViews($scope)
                            .then(function (data) {

                                if (data.data.errorData != null && data.data.errorData == "true") {
                                    $scope.publicNoticeSearch.errorMessage = data.errorMessage;
                                    $scope.publicNoticeSearch.errorContainerClass = "displayed-section";
                                    console.log("Request to server to record view counts failed.");
                                }
                                else {
                                    var dataArray = [];
                                    angular.forEach(data.data, function ( viewCountReceipt ) {

                                        var idx = $scope.publicNoticeSearch.notices.findIndex( function ( notice ) {
                                            return notice.notice.ad_id == viewCountReceipt ;
                                        });
                                        if ( idx < 0 ) {
                                            console.log('Unexpected index of -1 in recordNoticeView');
                                        }
                                        else {
                                            $scope.publicNoticeSearch.notices[ idx ].viewsReported = true;
                                        }

                                    });
                                }

                            }, function (data) {
                                console.log('Error on record view counts.');
                            });
                    }



                    $scope.publicNoticeSearch.filterNotices = function () {


                        // Don't do anything if the data has not arrived
                        if ($scope.publicNoticeSearch.notices == undefined) return;


                        var cats = $scope.publicNoticeSearch.request.selectedNoticeCategories;
                        var counties = $scope.publicNoticeSearch.request.selectedCounties;

                        var countyNames = [];
                        for ( var i = 0 ; i < counties.length ; i++ ) {
                            countyNames.push( counties[i].name );
                        }
                        
                        var cities =  $scope.publicNoticeSearch.request.selectedCities;


                        for (var i = 0; i < $scope.publicNoticeSearch.notices.length; i++) {

                            var passCategory = false;
                            var passDate = false;
                            var passKeywords = false;
                            var passCounty = false;
                            var passCity = false;

                            var notice = $scope.publicNoticeSearch.notices[i];
                           
                            if (cats.length === 0 || cats.indexOf(notice.category) >= 0) {
                                passCategory = true;
                            }
                            else {
                                passCategory = false;
                            }


                            if (countyNames.length === 0 || countyNames.indexOf(notice.notice.county ) >= 0) {
                                passCounty = true;
                            }
                            else {
                                passCounty = false;
                            }

                            if (cities.length === 0 || cities.indexOf(notice.notice.city) >= 0) {
                                passCity = true;
                            }
                            else {
                                passCity = false;
                            }


                            // Check date range
                            var fromDate = $scope.publicNoticeSearch.minDate;
                            var toDate = $scope.publicNoticeSearch.maxDate;

                            if ( $scope.publicNoticeSearch.request.fromDate == undefined ) {
                                $scope.publicNoticeSearch.request.fromDate = ''
                            }

                            if ( $scope.publicNoticeSearch.request.toDate == undefined ) {
                                $scope.publicNoticeSearch.request.toDate = ''
                            }

                            if ($scope.publicNoticeSearch.request.fromDate instanceof Date || $scope.publicNoticeSearch.request.fromDate.length != 0) {
                                fromDate = $scope.publicNoticeSearch.request.fromDate;
                            }

                            if ($scope.publicNoticeSearch.request.toDate instanceof Date || $scope.publicNoticeSearch.request.toDate.length != 0) {
                                toDate = $scope.publicNoticeSearch.request.toDate;
                            }

                            var noticeStartDate = new Date(notice.notice.ad_startdate);

                            if (noticeStartDate >= fromDate && noticeStartDate < toDate) {
                                passDate = true;
                            }
                            else {
                                passDate = false;
                            }

                            var keywords = $scope.publicNoticeSearch.request.keywords;

                            var responsibleEntity = notice.notice.responsible_entity_companyorganization;
                            if ( responsibleEntity == null || responsibleEntity == undefined ) responsibleEntity = '';

                            if (keywords.length == 0
                                || notice.strippedDetails.toLowerCase().indexOf(keywords.toLowerCase()) >= 0
                                || notice.notice.ad_title.toLowerCase().indexOf(keywords.toLowerCase()) >= 0
                                || responsibleEntity.toLowerCase().indexOf(keywords.toLowerCase()) >= 0) {
                                passKeywords = true;
                            }
                            else {
                                passKeywords = false;
                            }

                            notice.show = passCategory && passDate && passKeywords && passCounty && passCity ? true : false;

                            $scope.publicNoticeSearch.updateFilterFeedback();

                        }
                    }

                    $scope.publicNoticeSearch.showNoticeDetail = function (notice) {

                        // TODO: this can probably be removed.
                        // $scope.details = $scope.publicNoticeSearch.selectedNotice.notice.ad_details;


                        notice.listAsCollapsed = false;
                        notice.showListPanel = false;
                        if ( notice.viewCount == 0 ) notice.viewCount++;

                    };

                    $scope.publicNoticeSearch.hideNoticeDetail = function() {
                        
                    }

                    $scope.publicNoticeSearch.displayModalImage = function ( notice, index ) {
                        $scope.publicNoticeSearch.currentModalImageURL = notice.thumbsLarge[ index ];
                        
                        var modalInstance;
                        $scope.noticeDetailModal = modalInstance = $uibModal.open({
                            templateUrl: wpNg.config.modules.publicNotices.partialUrl + 'modal-image.html',
                            scope: $scope,
                            windowTopClass: 'image-modal'

                        });
                        $scope.close = function () {
                            modalInstance.close();
                        };
                    };




                    $scope.publicNoticeSearch.countyChecked= function( county, checked ) {

                        $scope.publicNoticeSearch.countyCollapses[ county.name  ] = ! checked ;
                        $scope.publicNoticeSearch.filterNotices();
                    }

                    $scope.publicNoticeSearch.cityChecked= function( city, checked ) {
                        $scope.publicNoticeSearch.filterNotices();
                    }


                    $scope.publicNoticeSearch.updateFilterFeedback = function() {


                        $scope.publicNoticeSearch.filterFeedback.reset();

                        var cats = $scope.publicNoticeSearch.request.selectedNoticeCategories ;
                        var cities = $scope.publicNoticeSearch.request.selectedCities ;
                        var counties = $scope.publicNoticeSearch.request.selectedCounties ;
                        var keywords = $scope.publicNoticeSearch.request.keywords ;


                        
                        var catFilterOn = cats.length > 0 ;
                        var cityFilterOn = cities.length > 0 ;
                        var countyFilterOn =  counties.length > 0;
                        var keywordsFilterOn =  keywords.length > 0 ;
                        var dateRangeFilterOn = false;
                        var fromDate = '';
                        var toDate = '';



                        if ($scope.publicNoticeSearch.request.fromDate instanceof Date || $scope.publicNoticeSearch.request.fromDate.length != 0) {
                            dateRangeFilterOn = true;
                            var d = $scope.publicNoticeSearch.request.fromDate;
                            fromDate = d.toLocaleDateString();
                        }


                        if ($scope.publicNoticeSearch.request.toDate instanceof Date || $scope.publicNoticeSearch.request.toDate.length != 0) {
                            dateRangeFilterOn = true;
                            var d = $scope.publicNoticeSearch.request.toDate;
                            toDate = d.toLocaleDateString();
                        }
                        

                        if (catFilterOn || cityFilterOn || countyFilterOn || keywordsFilterOn || dateRangeFilterOn ) {


                            if ( catFilterOn) {

                                var catString = '';
                                var count = cats.length;
                                for( var i = 0 ; i < count ; i++ ) {
                                    catString += cats[i];
                                    if ( i < count - 1  ) {
                                        catString += ', ';
                                    }
                                }
                                $scope.publicNoticeSearch.filterFeedback.categories = catString ;
                                $scope.publicNoticeSearch.filterFeedback.catFilterOn = true;
                                
                            }

                            if ( countyFilterOn ) {

                                var countyString = '';
                                var count = counties.length;
                                for( var i = 0 ; i < count ; i++ ) {
                                    countyString += counties[i].name;
                                    if ( i < count - 1  ) {
                                        countyString += ', ';
                                    }
                                }
                                $scope.publicNoticeSearch.filterFeedback.counties = countyString ;
                                $scope.publicNoticeSearch.filterFeedback.countyFilterOn = true;
                            }

                            if ( cityFilterOn ) {

                                var cityString = '';
                                var count = cities.length;
                                for( var i = 0 ; i < count ; i++ ) {
                                    cityString += cities[i] ;
                                    if ( i < count - 1  ) {
                                        cityString += ', ';
                                    }
                                }
                                $scope.publicNoticeSearch.filterFeedback.cities = cityString ;
                                $scope.publicNoticeSearch.filterFeedback.cityFilterOn = true;

                            }

                            if ( dateRangeFilterOn ) {
                                $scope.publicNoticeSearch.filterFeedback.dateRange = fromDate + ' - ' + toDate ;
                                $scope.publicNoticeSearch.filterFeedback.dateRangeFilterOn = true;

                            }

                            if ( keywordsFilterOn ) {
                                $scope.publicNoticeSearch.filterFeedback.keywords = $scope.publicNoticeSearch.request.keywords ;
                                $scope.publicNoticeSearch.filterFeedback.keywordsFilterOn = true;

                            }

                            $scope.publicNoticeSearch.filterFeedback.defaultOn = false; ;

                        }
                        else {
                            $scope.publicNoticeSearch.filterFeedback.defaultOn = true; ;
                        }

                    }

                    // End controller methods

                    // Interval timer for view counts.
                    $scope.publicNoticeSearch.updateViewCountInterval = $interval( function( ){
                            $scope.publicNoticeSearch.recordNoticeViews();
                        },
                        30000
                    );


                    // Get first set of notices to display
                    $scope.publicNoticeSearch.searchNotices();
                    
                    // Watch
                    $scope.$watch('publicNoticeSearch.request.fromDate', function (newValue, oldValue) {
                        $scope.publicNoticeSearch.filterNotices();
                    });

                    // Watch
                    $scope.$watch('publicNoticeSearch.request.toDate', function (newValue, oldValue) {
                        $scope.publicNoticeSearch.filterNotices();
                    });

                }]);

    app.factory('searchService', function ($q, $http, $location) {
        'use strict';
        var service = {};

        service.searchNotices = function ($scope) {

            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: wpNg.config.ajaxUrl,
                params: {
                    'action': 'pad_child_ajax',
                    'fn': 'search_notices',
                    'searchCriteria': $scope.publicNoticeSearch.request
                }

            })
                .then(function (data) {
                    deferred.resolve(data);
                }) ,(function (data) {
                    deferred.reject('There was an error in search notices!');
                });
            return deferred.promise;
        };

        return service;
    });

    app.factory('viewCountService', function ($q, $http, $location) {
        'use strict';
        var service = {};

        service.recordNoticeViews = function ($scope) {

            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: wpNg.config.ajaxUrl,
                params: {
                    'action': 'pad_child_ajax',
                    'fn': 'record_notice_views',
                    'noticesViewed': JSON.stringify( $scope.publicNoticeSearch.noticesViewed )
                }

            })
                .then(function (data) {
                    deferred.resolve(data);
                }) ,(function (data) {
                deferred.reject('There was an error in search notices!');
            });
            return deferred.promise;
        };

        return service;
    });


    app.directive('checklistModel', ['$parse', '$compile', function ($parse, $compile) {
        // contains
        function contains(arr, item, comparator) {
            if (angular.isArray(arr)) {
                for (var i = arr.length; i--;) {
                    if (comparator(arr[i], item)) {
                        return true;
                    }
                }
            }
            return false;
        }

        // add
        function add(arr, item, comparator) {
            arr = angular.isArray(arr) ? arr : [];
            if (!contains(arr, item, comparator)) {
                arr.push(item);
            }
            return arr;
        }

        // remove
        function remove(arr, item, comparator) {
            if (angular.isArray(arr)) {
                for (var i = arr.length; i--;) {
                    if (comparator(arr[i], item)) {
                        arr.splice(i, 1);
                        break;
                    }
                }
            }
            return arr;
        }

        // http://stackoverflow.com/a/19228302/1458162
        function postLinkFn(scope, elem, attrs) {
            // exclude recursion, but still keep the model
            var checklistModel = attrs.checklistModel;
            attrs.$set("checklistModel", null);
            // compile with `ng-model` pointing to `checked`
            $compile(elem)(scope);
            attrs.$set("checklistModel", checklistModel);

            // getter / setter for original model
            var getter = $parse(checklistModel);
            var setter = getter.assign;
            var checklistChange = $parse(attrs.checklistChange);
            var checklistBeforeChange = $parse(attrs.checklistBeforeChange);

            // value added to list
            var value = attrs.checklistValue ? $parse(attrs.checklistValue)(scope.$parent) : attrs.value;


            var comparator = angular.equals;

            if (attrs.hasOwnProperty('checklistComparator')) {
                if (attrs.checklistComparator[0] == '.') {
                    var comparatorExpression = attrs.checklistComparator.substring(1);
                    comparator = function (a, b) {
                        return a[comparatorExpression] === b[comparatorExpression];
                    };

                } else {
                    comparator = $parse(attrs.checklistComparator)(scope.$parent);
                }
            }

            // watch UI checked change
            scope.$watch(attrs.ngModel, function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }

                if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
                    scope[attrs.ngModel] = contains(getter(scope.$parent), value, comparator);
                    return;
                }

                setValueInChecklistModel(value, newValue);

                if (checklistChange) {
                    checklistChange(scope);
                }
            });

            function setValueInChecklistModel(value, checked) {
                var current = getter(scope.$parent);
                if (angular.isFunction(setter)) {
                    if (checked === true) {
                        setter(scope.$parent, add(current, value, comparator));
                    } else {
                        setter(scope.$parent, remove(current, value, comparator));
                    }
                }

            }

            // declare one function to be used for both $watch functions
            function setChecked(newArr, oldArr) {
                if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
                    setValueInChecklistModel(value, scope[attrs.ngModel]);
                    return;
                }
                scope[attrs.ngModel] = contains(newArr, value, comparator);
            }

            // watch original model change
            // use the faster $watchCollection method if it's available
            if (angular.isFunction(scope.$parent.$watchCollection)) {
                scope.$parent.$watchCollection(checklistModel, setChecked);
            } else {
                scope.$parent.$watch(checklistModel, setChecked, true);
            }
        }

        return {
            restrict: 'A',
            priority: 1000,
            terminal: true,
            scope: true,
            compile: function (tElement, tAttrs) {
                if ((tElement[0].tagName !== 'INPUT' || tAttrs.type !== 'checkbox') && (tElement[0].tagName !== 'MD-CHECKBOX') && (!tAttrs.btnCheckbox)) {
                    throw 'checklist-model should be applied to `input[type="checkbox"]` or `md-checkbox`.';
                }

                if (!tAttrs.checklistValue && !tAttrs.value) {
                    throw 'You should provide `value` or `checklist-value`.';
                }

                // by default ngModel is 'checked', so we set it if not specified
                if (!tAttrs.ngModel) {
                    // local scope var storing individual checkbox model
                    tAttrs.$set("ngModel", "checked");
                }

                return postLinkFn;
            }
        };
    }]);

    app.directive('noticeListing', function ($timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                publicNoticeSearch: '='
            },
            link: function (scope, elem, attrs) {

            },
            templateUrl: wpNg.config.modules.publicNotices.partialUrl + 'notice-listing.html'

        };
    });

    app.directive('noticeDetail', [ '$sce', '$timeout', function ( $sce, $timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                notice: '=notice',
                publicNoticeSearch: '='
            },
            link: function (scope, elem, attrs) {
                scope.close = function() {
                    scope.notice.listAsCollapsed = true;
                    scope.notice.showListPanel = true;
                }

            },
            templateUrl: wpNg.config.modules.publicNotices.partialUrl + 'notice-detail.html'

        };
    }]);

})(angular, wpNg);


